PolyClipping
======
C++ part is taken from [Clipper](http://www.angusj.com/delphi/clipper.php) - an open source freeware library for
clipping and offsetting lines and polygons by [Angus Johnson](https://github.com/AngusJohnson).